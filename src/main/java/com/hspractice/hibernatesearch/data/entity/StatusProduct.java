package com.hspractice.hibernatesearch.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Indexed
@Table(name = "status_product")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class StatusProduct{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Field(name = "description", index = Index.YES, analyze = Analyze.NO)
    @ContainedIn
    private String description;

    public StatusProduct() {
    }

    public StatusProduct(Long id, String  description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
