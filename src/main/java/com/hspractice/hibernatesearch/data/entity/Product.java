package com.hspractice.hibernatesearch.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Indexed
@Table(name = "product")
@ApiModel(description = "Modelo que representa a un producto")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product implements Serializable{
    @Id
    private Long id;

    @Field(name = "description", index = Index.YES, analyze = Analyze.NO, store = Store.YES)
    private String description;

    @IndexedEmbedded
    @ManyToOne(fetch = FetchType.LAZY)
    private StatusProduct statusProduct;

    public Product() {

    }

    public Product(String description, StatusProduct statusProduct) {
        this.description = description;
        this.statusProduct = statusProduct;
    }

    public Product(Long id, String description, StatusProduct statusProduct) {
        this.id = id;
        this.description = description;
        this.statusProduct = statusProduct;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatusProduct getStatusProduct() {
        return statusProduct;
    }

    public void setStatusProduct(StatusProduct statusProduct) {
        this.statusProduct = statusProduct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
