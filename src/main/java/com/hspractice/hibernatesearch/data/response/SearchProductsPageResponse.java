package com.hspractice.hibernatesearch.data.response;

import com.hspractice.hibernatesearch.data.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public class SearchProductsPageResponse {
	
	private Page<Product> products;

	public Page<Product> getProducts() {
		return products;
	}

	public void setProducts(Page<Product> products) {
		this.products = products;
	}

}
