package com.hspractice.hibernatesearch.data.repository;

import com.hspractice.hibernatesearch.data.entity.StatusProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StatusProductRepository extends JpaRepository<StatusProduct, Long>{

}
