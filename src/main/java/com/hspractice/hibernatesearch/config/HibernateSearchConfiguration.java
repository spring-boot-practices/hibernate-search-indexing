package com.hspractice.hibernatesearch.config;

import com.hspractice.hibernatesearch.service.HibernateSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Configuration
public class HibernateSearchConfiguration {

    @Value("${hibernate.search.enabled}")
    private boolean enabled;

    @PersistenceContext
    @Autowired
    private EntityManager entityManager;


    @Bean(name = "hibernateServiceServiceConfig")
    HibernateSearchService hibernateSearchService() {

        HibernateSearchService hibernateSearchService = new HibernateSearchService(entityManager);
        if(enabled){
            hibernateSearchService.initializeHibernateSearch();

        }
        return hibernateSearchService;

    }}
