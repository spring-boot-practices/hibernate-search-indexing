package com.hspractice.hibernatesearch.service;

import com.hspractice.hibernatesearch.data.entity.Product;
import com.hspractice.hibernatesearch.data.response.SearchProductsPageResponse;
import org.apache.lucene.search.Query;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.query.engine.spi.FacetManager;
import org.hibernate.search.query.facet.Facet;
import org.hibernate.search.query.facet.FacetSelection;
import org.hibernate.search.query.facet.FacetSortOrder;
import org.hibernate.search.query.facet.FacetingRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HibernateSearchService {

    @PersistenceContext
    @Autowired
    private final EntityManager entityManager;

    @Autowired
    public HibernateSearchService(EntityManager entityManager) {
        super();
        this.entityManager = entityManager;
    }

    public void initializeHibernateSearch() {

        try {
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager.getEntityManagerFactory().createEntityManager());
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public SearchProductsPageResponse search(
            String q,
            int page,
            int size,
            String[] attrs){

        attrs = (attrs == null) ? new String[]{} : attrs;
        page = (page < 0) ? 0 : page;
        FullTextEntityManager fullTextEntityManager = getFullTextEntityManager();
        Query searchQuery = this.getSearchQuery(q);

        FullTextQuery query = (FullTextQuery) fullTextEntityManager.createFullTextQuery(searchQuery, Product.class);

        int total = query.getResultSize();
        query.setFirstResult(page * size);
        query.setMaxResults(size);
        Pageable pageable = PageRequest.of(page, size);
        SearchProductsPageResponse searchProductsPageResponse = new SearchProductsPageResponse();

        searchProductsPageResponse.setProducts(
                new PageImpl<Product>(query.getResultList(), pageable, total).map(product -> {
                    return product;
                })
        );

        return searchProductsPageResponse;
    }

    private FullTextEntityManager getFullTextEntityManager() {
        return Search.getFullTextEntityManager(entityManager);
    }


    private QueryBuilder getQueryBuilder( ) {
        FullTextEntityManager fullTextEntityManager = getFullTextEntityManager();

        return fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder().forEntity(Product.class).get();
    }

    private Query getSearchQuery(String q) {
        return getQueryBuilder()
                .bool()
                .must(getQueryBuilder()
                        .keyword()
                        .onFields("description")
                        .matching(q)
                        .createQuery()
                )
                .must(getQueryBuilder()
                        .keyword()
                        .onField("statusProduct.description")
                        .matching("Publicado")
                        .createQuery()
                )
                .createQuery();
    }

    private void setFacetsSelection(FacetSelection facetSelection, List<Facet> facets, List<String> facetsNames) {
        List<Facet> facetsToSelect = facets.stream().filter(facet -> {
            return  facetsNames.stream().anyMatch(facetName -> {
                return facetName.equals(facet.getValue());
            });
        }).collect(Collectors.toList());

        for (Facet facet : facetsToSelect) {
            facetSelection.selectFacets(facet);
        }
    }

}
