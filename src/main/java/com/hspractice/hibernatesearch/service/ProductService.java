package com.hspractice.hibernatesearch.service;

import com.hspractice.hibernatesearch.data.entity.Product;

public interface ProductService {
    Product findById(long id);

    void updateProduct(Product p, Product req);
}
