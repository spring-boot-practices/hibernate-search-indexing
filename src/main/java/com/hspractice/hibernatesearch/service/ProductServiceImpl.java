package com.hspractice.hibernatesearch.service;

import com.hspractice.hibernatesearch.data.entity.Product;
import com.hspractice.hibernatesearch.data.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product findById(long id) {
        return productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("product.id"));
    }

    @Override
    public void updateProduct(Product p, Product req) {
        p.setDescription(req.getDescription());
        p.setStatusProduct(req.getStatusProduct());
        productRepository.saveAndFlush(p);
    }
}
