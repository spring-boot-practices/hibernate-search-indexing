package com.hspractice.hibernatesearch.controller;

import com.hspractice.hibernatesearch.data.entity.Product;
import com.hspractice.hibernatesearch.data.response.SearchProductsPageResponse;
import com.hspractice.hibernatesearch.service.HibernateSearchService;
import com.hspractice.hibernatesearch.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private HibernateSearchService hibernateSearchService;

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/decorator/product/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateDecoratorDetail(@PathVariable("id") long id, @RequestBody Product req)  {
        Product p = productService.findById(id);
        productService.updateProduct(p,req);
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SearchProductsPageResponse search(
            @RequestParam("query") String q,
            @RequestParam("page") int page,
            @RequestParam("size") int size,
            @RequestParam(value="attr", required = false, defaultValue="") String[] attrs){
        return this.hibernateSearchService.search(q, page, size, attrs);
    }
}

