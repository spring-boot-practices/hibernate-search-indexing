package com.hspractice.hibernatesearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernatesearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernatesearchApplication.class, args);
	}

}
