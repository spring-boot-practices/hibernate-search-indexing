insert into status_product (id, description) values (1, 'Publicado');
insert into status_product (id, description) values (2, 'Borrador');

insert into product (id, description, status_product_id) values (1,'pollo',1);
insert into product (id, description, status_product_id) values (2,'fideos',1);
insert into product (id, description, status_product_id) values (3,'pollo asado',2);

